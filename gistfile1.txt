      # URL shortener
      location /newcomer  { return 302 http://docs.coala.io/en/latest/Developers/Newcomers_Guide.html; }
      location /newcomers { return 302 http://docs.coala.io/en/latest/Developers/Newcomers_Guide.html; }
      location /new       { return 302 https://github.com/issues?utf8=%E2%9C%93&q=is%3Aopen+is%3Aissue+user%3Acoala+label%3Adifficulty%2Fnewcomer++no%3Aassignee; }
      location /low       { return 302 https://github.com/issues?utf8=%E2%9C%93&q=is%3Aopen+is%3Aissue+user%3Acoala+label%3Adifficulty%2Flow++no%3Aassignee; }
      location /review    { return 302 https://github.com/pulls?q=is%3Aopen+is%3Apr+user%3Acoala+label%3A%22process%2Fpending+review%22; }
      location /languages { return 302 https://github.com/coala/bear-docs/blob/master/README.rst; }
      location /chat      { return 302 https://gitter.im/coala-analyzer/coala; }
      location /git       { return 302 http://docs.coala.io/en/latest/Developers/Git_Basics.html; }
      location /commit    { return 302 http://docs.coala.io/en/latest/Developers/Writing_Good_Commits.html; }
      location /cep       { return 302 https://github.com/coala/cEPs/blob/master/cEP-0000.md; }
      location /tutorial  { return 302 https://docs.coala.io/en/latest/Users/Tutorial.html; }
      location /writingbears { return 302 https://docs.coala.io/en/latest/Developers/Writing_Bears.html; }
      location /channels  { return 302 https://github.com/coala/coala/wiki/Communication-Channels; }
      location /newform   { return 302 https://docs.google.com/forms/d/e/1FAIpQLSd7g_MU_c-BMQ62WHeznrvcoXwqW87O_Wq4Gz7-pp8PJ38Wdg/viewform; }
      location /projects  { return 302 https://github.com/coala/coala/wiki/Project-Ideas; }
      location /reviewsprint { return 302 https://docs.google.com/forms/d/e/1FAIpQLSd4vHafTyY4RW--fOyIVecBM0WKNEeF-RyFvUn83jCF9ou2tg/viewform; }
      location /reply     { return 302 https://github.com/coala/coala/wiki/Reply-Templates; }
